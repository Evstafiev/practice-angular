import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {UserListComponent} from './user-list.component';
import {UserDetailComponent} from './user-detail.component';
import {UserService} from './user.service';
import {UserRoutingModule} from './user-routing.module';
import {UserCreateComponent} from './user-create.component';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        UserRoutingModule
    ],
    declarations: [
        UserListComponent,
        UserDetailComponent,
        UserCreateComponent,
    ],
    providers: [UserService]
})
export class UsersModule {
}
