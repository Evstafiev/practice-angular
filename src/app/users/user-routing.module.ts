import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {UserListComponent} from './user-list.component';
import {UserDetailComponent} from './user-detail.component';
import {UserCreateComponent} from './user-create.component';
const userRoutes: Routes = [
    {path: 'users', component: UserListComponent},
	{path: 'reg', component: UserCreateComponent},   
    {path: 'user/:id', component: UserDetailComponent}
];

@NgModule({
    imports: [
        RouterModule.forChild(userRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class UserRoutingModule {
}
