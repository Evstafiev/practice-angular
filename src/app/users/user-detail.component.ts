import 'rxjs/add/operator/switchMap';
import {Component, OnInit, HostBinding} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Router, ActivatedRoute, ParamMap, Params} from '@angular/router';

import {User, UserService} from './user.service';

@Component({
    templateUrl: './user-detail.component.html'
})
export class UserDetailComponent implements OnInit {
    @HostBinding('style.display') display = 'block';
    @HostBinding('style.position') position = 'absolute';
    isEditState: boolean = false;

    user: User;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private service: UserService) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => this.loadUser(params));
    }

    private loadUser(params: Params) {
        this.service.getUser(params['id'])
            .subscribe(
                user => this.user = user,
                err => console.log(err)
            )
    }

    goToUsers(user: User) {
        let id = user ? user.id : null;
        this.router.navigate(['/users', {id}]);
    }

    toggleEditState() {
        this.isEditState = !this.isEditState;
    }

    save() {
        this.service.updateUser(this.user)
            .subscribe();

    }
    delete() {
        this.service.deleteUser(this.user.id)
            .subscribe();
        this.router.navigate(['/users']);
    }


}
