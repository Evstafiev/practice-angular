import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, RequestOptionsArgs, Response} from '@angular/http';
import {ResponseContentType} from '@angular/http/src/enums';

export class User {
    constructor(public id: number,
                public name: string,
                public age: number,
                public address: string,
                public fruit: string) {
    }
}

@Injectable()
export class UserService {
    private userUrl = 'http://localhost:3000/';

    constructor(private http: Http) {
    }

    getUsers(name?: string): Observable<User[]> {
        return this.http.get(this.userUrl, {params: {name}})
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    getUser(id: number | string) {
        return this.http.get(`${this.userUrl}${+id}`)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

    updateUser(user: User) {
        return this.http.put(`${this.userUrl}${+user['id']}`, user)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    createUser(user: User) {
        return this.http.post(`${this.userUrl}`, user)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    
    deleteUser(id: number) {
        return this.http.delete(`${this.userUrl}${+id}`)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }

}
