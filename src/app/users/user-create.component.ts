import 'rxjs/add/operator/switchMap';
import {Component, OnInit, HostBinding} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Router, ActivatedRoute, ParamMap, Params} from '@angular/router';

import {User, UserService} from './user.service';

@Component({
    templateUrl: './user-create.component.html'
})
export class UserCreateComponent /* implements OnInit */ {
    @HostBinding('style.display') display = 'block';
    @HostBinding('style.position') position = 'absolute';

    user: User = new User(null, null, null, null, null);

    constructor(private route: ActivatedRoute,
                private router: Router,
                private service: UserService) {
    }
    goToUsers() {
        this.router.navigate(['/users']);
    }

    saveuser() {
        this.service.createUser(this.user)
            .subscribe();
        let id = this.user ? this.user.id : null;
        this.router.navigate(['/users', {id}]);
    }

}
